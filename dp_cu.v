`timescale 1 ns/1 ns 
module fp_adder(input [31:0]abus, bbus, input add_or_sub, clk, rst, start, output [31:0]outbus, output ready);
	wire ld_a, ld_b, ld_e, ld_sm, en_sm, ld_sa, ld_inc, ld_out;
	DP d(abus, bbus, add_or_sub, ld_a, ld_b, ld_e, ld_sm, en_sm, ld_sa, ld_inc, ld_out, clk, rst, outbus);
	CU c(rst, clk, start, ld_a, ld_b, ld_e, ld_sm, en_sm, ld_sa, ld_inc, ld_out, ready);
endmodule