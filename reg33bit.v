`timescale 1 ns/1 ns 
module reg33(input [31:0] reg_in, input ld, clk, rst, output reg[32:0] reg_out);
	always @(posedge clk, posedge rst) begin
		if(rst) reg_out <= 33'b0;
		else if(ld) reg_out <= {reg_in[31:23], 1'b1, reg_in[22:0]};
	end
endmodule