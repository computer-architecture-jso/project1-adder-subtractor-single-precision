`timescale 1 ns/1 ns 
module ALU(input [23:0] in_a, in_b, input add_or_sub, sa, sb, sign_exp, output reg[23:0]out, output reg co, sign_out, special);
	wire comp_mant;
	always @(in_a, in_b, add_or_sub, sa, sb) begin
		special = 1'b0;
		if(~add_or_sub) begin
			if(~(sa^sb)) begin
				{co, out} = in_a + in_b;
				if(sign_exp) begin
					sign_out = (comp_mant == 2'b10) ? sb : sa ;
				end else begin
					sign_out = (comp_mant == 2'b10) ? sa : sb ;
				end
			end else begin
				special = 1'b1;
				out = (comp_mant == 2'b00) ? in_b - in_a : in_a - in_b;
				co = 1'b0;
				if(sign_exp) begin
					sign_out = (comp_mant == 2'b10) ? sa : ~sa ;
				end else begin
					sign_out = (comp_mant == 2'b10) ? ~sa : sa ;
				end
			end
		end	else begin
			if(~(sa^sb)) begin
				special = 1'b1;
				out = (comp_mant == 2'b00) ? in_b - in_a : in_a - in_b;
				co = 1'b0;
				if(sign_exp) begin
					sign_out = (comp_mant == 2'b10) ? sa : ~sa ;
				end else begin
					sign_out = (comp_mant == 2'b10) ? ~sa : sa ;
				end
			end else begin
				{co, out} = in_b + in_a;
				if(sign_exp) begin
					sign_out = (comp_mant == 2'b10) ? sa : sb ;
				end else begin
					sign_out = (comp_mant == 2'b10) ? sb : sa ;
				end
			end
		end

		// if(~add_or_sub) begin
		// 	if(sign_exp) begin // in_a -> A, sa , in_b -> B, sb  ==> in_a + in_b
		// 		if(~(sa^sb)) begin
		// 			{co, out} = in_a + in_b;
		// 			sign_out = sa;
		// 		end
		// 		if(sa == 1'b1 && sb == 1'b0) begin
		// 			out = in_b - in_a;
		// 			co = 1'b0;
		// 			sign_out = (comp_mant == 2'b10) ? 1'b0 : 1'b1;
		// 		end else if(sa == 1'b0 && sb == 1'b1) begin
		// 			out = in_a - in_b;
		// 			co = 1'b0;
		// 			sign_out = (comp_mant == 2'b10) ? 1'b1 : 1'b0;
		// 		end
		// 	end else begin // in_a -> B, sb , in_b -> A, sa	 ==> in_a + in_b
		// 		if(~(sa^sb)) begin
		// 			{co, out} = in_a + in_b;
		// 			sign_out = sa;
		// 		end
		// 		if(sa == 1'b1 && sb == 1'b0) begin
		// 			out = in_a - in_b;
		// 			co = 1'b0;
		// 			sign_out = (comp_mant == 2'b10) ? 1'b1 : 1'b0;
		// 		end else if(sa == 1'b0 && sb == 1'b1) begin
		// 			out = in_b - in_a;
		// 			co = 1'b0;
		// 			sign_out = (comp_mant == 2'b10) ? 1'b0 : 1'b1;
		// 		end
		// 	end
		// end else begin
		// 	if(sign_exp) begin // in_a -> A, sa , in_b -> B, sb  ==> in_a - in_b
		// 		if(~(sa^sb)) begin
		// 			out = (comp_mant == 2'b00) ? in_a - in_b : in_b - in_a;
		// 			co = 1'b0;
		// 			sign_out = (comp_mant == 2'b10) ? sa : ~sa;
		// 		end
		// 		if(sa == 1'b1 && sb == 1'b0) begin
		// 			{co, out} = in_a + in_b;
		// 			sign_out = 1'b1;
		// 		end else if(sa == 1'b0 && sb == 1'b1) begin
		// 			{co, out} = in_a + in_b;
		// 			sign_out = 1'b0;
		// 		end
		// 	end else begin // in_a -> B, sb , in_b -> A, sa  ==> in_a - in_b
		// 		if(~(sa^sb)) begin
		// 			out = (comp_mant == 2'b10) ? in_b - in_a : in_a - in_b;
		// 			co = 1'b0;
		// 			sign_out = (comp_mant == 2'b00) ? sa : ~sa;
		// 		end
		// 		if(sa == 1'b1 && sb == 1'b0) begin
		// 			{co, out} = in_a + in_b;
		// 			sign_out = 1'b1;
		// 		end else if(sa == 1'b0 && sb == 1'b1) begin
		// 			{co, out} = in_a + in_b;
		// 			sign_out = 1'b0;
		// 		end
		// 	end
		// end
	end
	assign comp_mant = (in_b > in_a) ? 2'b10 : 
						(in_b == in_a) ? 2'b01 : 2'b00;
endmodule
