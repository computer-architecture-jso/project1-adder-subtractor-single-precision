`timescale 1 ns/1 ns 
module CU(input rst, clk, start, output reg ld_a, ld_b, ld_e, ld_sm, en_sm, ld_sa, ld_inc, ld_out, ready);
	parameter [2:0] idle = 0, inputs = 1, exp_diff = 2, mantissa_shift = 3, st4 = 4, correctors = 5, get_output = 6;
	reg [2:0] Ps, Ns;
	always @(start, Ps) begin
		{ld_a, ld_b, ld_e, ld_sm, en_sm, ld_sa, ld_inc, ld_out, ready} = 9'b0;
		case(Ps)
			idle : begin
				if(start)
					Ns = inputs;
				else begin
					Ns = idle;
				end
				ready = 1;
			end

			inputs : begin
				Ns = exp_diff;
				ld_a = 1;
				ld_b = 1;
			end

			exp_diff : begin
				Ns = mantissa_shift;
				ld_e = 1;
			end

			mantissa_shift : begin
				Ns = st4;
				ld_sm = 1;
				en_sm = 1;
			end

			st4 : begin
				Ns = correctors;
			end

			correctors : begin
				Ns = get_output;
				ld_sa = 1;
				ld_inc = 1;
			end

			get_output : begin
				Ns = idle;
				ld_out = 1;
			end

			default : Ns = idle;
		endcase
	end

	always @(posedge clk, posedge rst) begin
		if(rst) Ps <= 3'b0;
		else Ps <= Ns;
	end
endmodule