`timescale 1 ns/1 ns 
module onebit_shifter(input [23:0] in, input ld, co, special, clk, rst, output reg[23:0] out);
	always@(posedge clk, posedge rst) begin
		if(rst)	out <= 24'b0;
		else if(ld) out <= in;
		if(co) out <= in >> co;
		if(special) out <= in << 1'b1;
	end
endmodule