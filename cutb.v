`timescale 1 ns/1 ns 
module CUTB();
reg rst, start;
reg clk = 0;
wire ld_a, ld_b, ld_e, ld_sm, en_sm, ld_sa, ld_inc, ld_out, ready;

always #15
	clk = ~clk;

CU c(rst, clk, start, ld_a, ld_b, ld_e, ld_sm, en_sm, ld_sa, ld_inc, ld_out, ready);
initial begin
	rst=1;
	#60;
	rst=0;
	start=0;
	#60;
	start=1;
	#400;
	$stop;
end
endmodule