`timescale 1 ns/1 ns 
module DP(input [31:0]a, b, input add_or_sub, ld_a, ld_b, ld_e, ld_sm, en_sm, ld_sa, ld_inc, ld_out, clk, rst, output [31:0]out);
	wire [32:0] op_a_out, op_b_out;
	wire [7:0] e_mux_out, exp_mux_ls_out, exp_mux_gt_out, e_diff, e_out;
	wire [23:0] m1_mux_out, m2_mux_out, shifted_out, alu_out, mantissa_out;
	wire sign_e, co, sign_out, special;
	reg33 op_a_reg(a, ld_a, clk, rst, op_a_out);
	reg33 op_b_reg(b, ld_b, clk, rst, op_b_out);
	exp_diff exp_diff_reg({1'b0, op_a_out[31:24]}, {1'b0, op_b_out[31:24]}, ld_e, clk ,rst, e_diff, sign_e);
	mux24 m1_mux(op_a_out[23:0], op_b_out[23:0], sign_e, m1_mux_out);
	mux24 m2_mux(op_b_out[23:0], op_a_out[23:0], sign_e, m2_mux_out);
	mux8 exp_mux_gt(op_b_out[31:24], op_a_out[31:24], sign_e, exp_mux_gt_out);
	mux8 exp_mux_ls(op_a_out[31:24], op_b_out[31:24], sign_e, exp_mux_ls_out);
	mux8 exp_mux_end(exp_mux_ls_out, exp_mux_gt_out, special, e_mux_out);
	mantissa_shifter shift_mantissa(m1_mux_out, e_diff, ld_sm, en_sm, clk, rst, shifted_out);
	ALU alu(shifted_out, m2_mux_out, add_or_sub, op_a_out[32], op_b_out[32], sign_e, alu_out, co, sign_out, special);
	onebit_shifter obit_sh(alu_out, ld_sa, co, special, clk, rst, mantissa_out);
	inc_exp inc_expo(e_mux_out, ld_inc, co, clk, rst, e_out);
	reg32 out_reg({sign_out, e_out, mantissa_out[22:0]}, ld_out, clk, rst, out);
endmodule
