`timescale 1 ns/1 ns 
module mux8(input [7:0]in_a, in_b, input sel, output [7:0]out);
	assign out = (sel) ? in_a : in_b;
endmodule