`timescale 1 ns/1 ns 
module inc_exp(input [7:0] in, input ld, co, clk, rst, output reg[7:0] out);
	always@(posedge clk, posedge rst) begin
		if(rst)	out <= 8'b0;
		else if(ld) out <= in;
		if(co) out <= in + 1;
	end
endmodule