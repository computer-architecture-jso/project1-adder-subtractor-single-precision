`timescale 1 ns/1 ns 
module exp_diff(input signed[8:0]exp_a, exp_b, input ld, clk, rst, output reg [7:0]exp_diff, output reg sign_d);
	reg [7:0]temp_out;
	always @(posedge clk, posedge rst) begin
		if(rst) begin
			exp_diff = 7'b0;
			sign_d = 1'bx;
		end
		else if(ld) begin
			{sign_d, temp_out} = exp_a - exp_b;
			if(sign_d) exp_diff = ~(temp_out) + 1'b1;
			else exp_diff = temp_out;
		end
	end
endmodule