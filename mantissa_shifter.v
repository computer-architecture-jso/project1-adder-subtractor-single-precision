`timescale 1 ns/1 ns 
module mantissa_shifter(input [23:0]in, input [7:0]sh_val, input ld, en, clk, rst, output reg[23:0]out);
	always@(posedge clk, posedge rst) begin
		if(rst)	out <= 24'b0;
		else if(ld) out <= in;
		if(en) out <= in >> sh_val;
	end
endmodule