`timescale 1 ns/1 ns 
module mux24(input [23:0]in_a, in_b, input sel, output [23:0]out);
	assign out = (sel) ? in_a : in_b;
endmodule