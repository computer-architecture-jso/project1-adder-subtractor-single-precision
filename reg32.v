`timescale 1 ns/1 ns 
module reg32(input [31:0] reg_in, input ld, clk, rst, output reg[31:0] reg_out);
	always @(posedge clk, posedge rst) begin
		if(rst) reg_out <= 32'b0;
		else if(ld) reg_out <= reg_in;
	end
endmodule