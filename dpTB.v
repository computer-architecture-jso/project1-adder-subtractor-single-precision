`timescale 1 ns/1 ns 
module DPTB();
reg [31:0]abus, bbus;
reg add_or_sub, ld_a, ld_b, ld_e, ld_sm, en_sm, ld_sa, ld_inc, ld_out, rst;
reg clk = 0;
wire [31:0]out;

always #15
	clk = ~clk;

DP d(abus, bbus, add_or_sub, ld_a, ld_b, ld_e, ld_sm, en_sm, ld_sa, ld_inc, ld_out, clk, rst, out);
initial begin
	// ADD
	abus=32'b01000000011000000000000000000000;
	bbus=32'b00111111110000000000000000000000;
	add_or_sub=0;
	ld_a=1;
	ld_b=1;
	#60;
	ld_a=0;
	ld_b=0;
	ld_e=1;
	#60;
	ld_e=0;
	ld_sm=1;
	en_sm=1;
	#60;
	ld_sm=0;
	en_sm=0;
	ld_sa=1;
	ld_inc=1;
	#60;
	ld_sa=0;
	ld_inc=0;
	ld_out=1;
	#300;
	$stop;
end
endmodule